import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { GameData } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class GameInfoService {
  public selectedGame$: BehaviorSubject<
    GameData
  > = new BehaviorSubject<GameData>({});
  constructor() {}
  gameSelected(data: GameData) {
    this.selectedGame$.next(data);
  }
}
