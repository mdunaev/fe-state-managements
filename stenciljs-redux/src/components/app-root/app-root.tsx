import { Component, Prop } from '@stencil/core';
import { Store } from '@stencil/redux';
import { createStore, applyMiddleware } from 'redux';
import { data } from './data';

// REDUCERS
const reducers = function(state, action) {
  console.log(action);
  switch (action.type) {
    case 'TOGGLE_PRESENTATION':
      return {
        ...state,
        isPresentation: !state.isPresentation
      };
    case 'GAME_SELECTED':
      return {
        ...state,
        selectedGame: action.payload
      };
    case 'DATA_LOADED':
      return {
        ...state,
        gamesList: action.payload
      };
    default:
      return state;
  }
};

// INITIAL STATE
const initialState: any = {
  isPresentation: false,
  selectedGame: {
    name: '',
    players: null,
    isNew: false
  },
  gamesList: []
};

// MIDDLEWARE
const httpMiddleware = () => next => action => {
  if (action.type === 'LOAD_DATA') {
    setTimeout(
      () => next({ type: 'DATA_LOADED', payload: data }),
      1000
    );
  } else {
    next(action);
  }
};

@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css'
})
export class AppRoot {
  // Get Store
  @Prop({ context: 'store' })
  store: Store;

  componentWillLoad() {
    // Configure Redux
    this.store.setStore(
      createStore(
        reducers,
        initialState,
        applyMiddleware(httpMiddleware)
      )
    );
    // Load data
    this.store.getStore().dispatch({
      type: 'LOAD_DATA'
    });
  }

  render() {
    return (
      <div>
        <div class="header">
          <div class="presentation_button">
            <app-switcher />
          </div>
        </div>
        <div class="dashboard">
          <app-games-list />
          <app-game-info />
        </div>
      </div>
    );
  }
}
