import { Component, OnInit } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-game-info',
  templateUrl: './game-info.component.html',
  styleUrls: ['./game-info.component.css']
})
export class GameInfoComponent implements OnInit {
  // Redux @select
  @select('isPresentation') isPresentation$: Observable<boolean>;
  @select('selectedGame') selectedGame$: Observable<boolean>;

  constructor() {}

  ngOnInit() {}
}
