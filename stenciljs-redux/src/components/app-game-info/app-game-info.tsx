import { Component } from '@stencil/core';

@Component({
  tag: 'app-game-info',
  styleUrl: 'app-game-info.css'
})
export class GameInfo {
  render = () => (
    <div class="game-info">
      <div class="content">
        <div class="is-new">new</div>
        <div class="game-info-name">First Name</div>

        <div class="num-players">420</div>
      </div>
    </div>
  );
}
