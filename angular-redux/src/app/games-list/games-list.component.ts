import { Component, OnInit } from '@angular/core';
import { select, NgRedux } from '@angular-redux/store';
import { Observable } from 'rxjs';
import { GameData } from '../data';

@Component({
  selector: 'app-games-list',
  templateUrl: './games-list.component.html',
  styleUrls: ['./games-list.component.css']
})
export class GamesListComponent implements OnInit {
  @select('isPresentation') isPresentation$: Observable<boolean>;
  @select('gamesList') gamesList$: Observable<boolean>;
  constructor(private redux: NgRedux<any>) {}

  ngOnInit() {}

  gameSelectedHandler(game: GameData) {
    this.redux.dispatch({
      type: 'GAME_SELECTED',
      payload: game
    });
  }
}
