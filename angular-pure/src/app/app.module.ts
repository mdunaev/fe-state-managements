import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SwitchBtnComponent } from './switch-btn/switch-btn.component';
import { GamesListComponent } from './games-list/games-list.component';
import { GameInfoComponent } from './game-info/game-info.component';

@NgModule({
  declarations: [
    AppComponent,
    SwitchBtnComponent,
    GamesListComponent,
    GameInfoComponent
  ],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
