import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  NgReduxModule,
  NgRedux,
  DevToolsExtension
} from '@angular-redux/store';

import { AppComponent } from './app.component';
import { SwitchBtnComponent } from './switch-btn/switch-btn.component';
import { GamesListComponent } from './games-list/games-list.component';
import { GameInfoComponent } from './game-info/game-info.component';
import { data } from './data';

// REDUCERS
const reducers = function(state, action) {
  switch (action.type) {
    case 'TOGGLE_PRESENTATION':
      return {
        ...state,
        isPresentation: !state.isPresentation
      };
    case 'GAME_SELECTED':
      return {
        ...state,
        selectedGame: action.payload
      };
    case 'DATA_LOADED':
      return {
        ...state,
        gamesList: action.payload
      };
    default:
      return state;
  }
};

// INITIAL STATE
const initialState = {
  isPresentation: false,
  selectedGame: {
    name: '',
    players: null,
    isNew: false
  },
  gamesList: []
};

// MIDDLEWARE
const httpMiddleware = store => next => action => {
  if (action.type === 'LOAD_DATA') {
    setTimeout(
      () => next({ type: 'DATA_LOADED', payload: data }),
      1000
    );
  } else {
    next(action);
  }
};

@NgModule({
  declarations: [
    AppComponent,
    SwitchBtnComponent,
    GamesListComponent,
    GameInfoComponent
  ],
  imports: [BrowserModule, NgReduxModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    private redux: NgRedux<any>,
    private devTools: DevToolsExtension
  ) {
    // CONFIGURE REDUX
    this.redux.configureStore(
      reducers,
      initialState,
      [httpMiddleware],
      [this.devTools.enhancer()] // Redux devtool
    );
    // START LOAD DATA
    this.redux.dispatch({
      type: 'LOAD_DATA'
    });
  }
}
