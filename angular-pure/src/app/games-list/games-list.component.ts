import { Component, OnInit } from '@angular/core';
import { HttpService, GameData } from '../http.service';
import { GameInfoService } from '../game-info.service';
import { PresentationService } from '../presentation.service';

@Component({
  selector: 'app-games-list',
  templateUrl: './games-list.component.html',
  styleUrls: ['./games-list.component.css']
})
export class GamesListComponent implements OnInit {
  constructor(
    private http: HttpService,
    private gameInfo: GameInfoService,
    private presentation: PresentationService
  ) {}

  ngOnInit() {}
}
