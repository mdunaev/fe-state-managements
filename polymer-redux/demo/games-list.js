import {
  PolymerElement,
  html
} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-icon/iron-icon.js';

class GamesList extends PolymerElement {
  static get template() {
    return html`
      <style>
      :host {
        display: inline-block;
        width: 50%;
        vertical-align: top;
      }

      .games-list {
        width: 100%;
        height: 400px;
        background-color: #ecf0f1;
        display: inline-block;
        vertical-align: top;
        padding: 10px;
        box-sizing: border-box;
      }

      ul {
        margin: 0;
        padding: 0;
      }
      
      li {
        padding: 10px;
        list-style-type: none;
        cursor: pointer;
        transition: background-color 0.3s ease, padding-left 0.3s ease,
          color 0.3s ease;
      }
      
      li:hover {
        background-color: #34495e;
        color: white;
        padding-left: 40px;
      }
      
      .players-num {
        display: none;
      }
      
      .presentation {
        width: 100%;
      }
      
      .presentation ul {
        display: flex;
        align-items: flex-end;
        height: 400px;
      }
      
      .presentation li {
        width: 10%;
        display: inline-block;
        background-color: #34495e;
        color: white;
        position: relative;
      }
      
      .presentation li:hover {
        padding-left: 10px;
      }
      
      .presentation .game-name {
        position: absolute;
        bottom: -110px;
        left: 20px;
        width: 145px;
        height: 100px;
        color: #34495e;
        font-size: 16px;
        transform: rotateZ(45deg);
        transform-origin: top left;
      }
      
      .presentation .players-num {
        display: block;
        position: absolute;
        top: -30px;
        font-size: 30px;
        color: #34495e;
      }
      
      @media (max-width: 800px) {
        :host {
          width: 100%;
        }
      }
      
      </style>
      <div class="games-list">
      [[test]]
        <ul>
          <template is="dom-repeat" items="[[list]]">
            <li>
              <div class="game-name">[[item.name]]</div>
              <div class="players-num">[[item.players]]</div>
            </li>
          </template>
        </ul>
      </div>
    `;
  }

  constructor() {
    super();
    this.list = [];
    window.store.subscribe(() => {
      this.list = window.store.getState().gamesList;
    });
  }
}

customElements.define('app-games-list', GamesList);
