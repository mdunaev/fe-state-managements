import { Component, OnInit } from '@angular/core';
import { PresentationService } from '../presentation.service';
import { GameInfoService } from '../game-info.service';

@Component({
  selector: 'app-game-info',
  templateUrl: './game-info.component.html',
  styleUrls: ['./game-info.component.css']
})
export class GameInfoComponent implements OnInit {
  constructor(
    private presentation: PresentationService,
    private gameInfo: GameInfoService
  ) {}

  ngOnInit() {}
}
