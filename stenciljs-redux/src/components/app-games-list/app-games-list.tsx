import { Component, Prop, State } from '@stencil/core';
import { Store } from '@stencil/redux';

@Component({
  tag: 'app-games-list',
  styleUrl: 'app-games-list.css'
})
export class GamesList {
  @Prop({ context: 'store' })
  store: Store;
  @State() isPresentation: boolean;
  @State() gamesList: any[];

  componentWillLoad() {
    this.store.mapStateToProps(this, state => {
      this.isPresentation = state.isPresentation;
      this.gamesList = state.gamesList;
      return state;
    });
  }

  render() {
    const classes = `games-list ${this.isPresentation &&
      'presentation'}`;
    return (
      <div class={classes}>
        <ul>
          {this.gamesList.map(game => (
            <li>
              <div class="game-name"> {game.name} </div>
              <div class="players-num"> {game.players} </div>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}
