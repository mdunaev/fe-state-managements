export interface GameData {
  name?: string;
  players?: number;
  isNew?: boolean;
}

export const data: GameData[] = [
  {
    name: 'Money Wheel',
    players: 162,
    isNew: false
  },
  {
    name: 'Roulette',
    players: 30,
    isNew: false
  },
  {
    name: 'Blackjack',
    players: 107,
    isNew: false
  },
  {
    name: 'Baccarat',
    players: 91,
    isNew: false
  },
  {
    name: 'Dragon Tiger',
    players: 32,
    isNew: true
  },
  {
    name: 'Casino Holdem',
    players: 211,
    isNew: false
  },
  {
    name: 'Caribbean stud',
    players: 133,
    isNew: false
  },
  {
    name: 'Ultimate Texas Holdem',
    players: 220,
    isNew: true
  },
  {
    name: 'Texas Holdem Bonus',
    players: 61,
    isNew: false
  },
  {
    name: 'Dual Play',
    players: 42,
    isNew: false
  }
];
