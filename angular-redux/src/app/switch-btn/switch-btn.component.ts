import { Component, OnInit } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

@Component({
  selector: 'app-switch-btn',
  templateUrl: './switch-btn.component.html',
  styleUrls: ['./switch-btn.component.css']
})
export class SwitchBtnComponent implements OnInit {
  constructor(private redux: NgRedux<any>) {}

  ngOnInit() {}

  clickHandler() {
    this.redux.dispatch({
      type: 'TOGGLE_PRESENTATION'
    });
  }
}
