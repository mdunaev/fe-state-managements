// REDUCERS
const reducers = function(state, action) {
  switch (action.type) {
    case 'TOGGLE_PRESENTATION':
      return {
        ...state,
        isPresentation: !state.isPresentation
      };
    case 'GAME_SELECTED':
      return {
        ...state,
        selectedGame: action.payload
      };
    case 'DATA_LOADED':
      return {
        ...state,
        gamesList: action.payload
      };
    default:
      return state;
  }
};

// INITIAL STATE
const initialState = {
  isPresentation: false,
  selectedGame: {
    name: '',
    players: null,
    isNew: false
  },
  gamesList: []
};

// MIDDLEWARE
const httpMiddleware = store => next => action => {
  if (action.type === 'LOAD_DATA') {
    setTimeout(
      () => next({ type: 'DATA_LOADED', payload: data }),
      1000
    );
  } else {
    next(action);
  }
};

window.store = Redux.createStore(
  reducers,
  initialState,
  Redux.applyMiddleware(httpMiddleware)
);
window.store.dispatch({
  type: 'LOAD_DATA'
});

// Mocked data
//
//
const data = [
  {
    name: 'Money Wheel',
    players: 162,
    isNew: false
  },
  {
    name: 'Roulette',
    players: 30,
    isNew: false
  },
  {
    name: 'Blackjack',
    players: 107,
    isNew: false
  },
  {
    name: 'Baccarat',
    players: 91,
    isNew: false
  },
  {
    name: 'Dragon Tiger',
    players: 32,
    isNew: true
  },
  {
    name: 'Casino Holdem',
    players: 211,
    isNew: false
  },
  {
    name: 'Caribbean stud',
    players: 133,
    isNew: false
  },
  {
    name: 'Ultimate Texas Holdem',
    players: 220,
    isNew: true
  },
  {
    name: 'Texas Holdem Bonus',
    players: 61,
    isNew: false
  },
  {
    name: 'Dual Play',
    players: 42,
    isNew: false
  }
];
