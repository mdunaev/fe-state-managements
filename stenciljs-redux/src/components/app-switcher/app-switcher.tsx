import { Component, Prop, Listen } from '@stencil/core';
import { Store, Action } from '@stencil/redux';

@Component({
  tag: 'app-switcher',
  styleUrl: 'app-switcher.css'
})
export class AppSwitcher {
  @Prop() name: string;
  @Prop({ context: 'store' })
  redux: Store;
  changeName: Action;

  @Listen('mouseup')
  clickHandler() {
    this.redux.getStore().dispatch({
      type: 'TOGGLE_PRESENTATION'
    });
  }

  render = () => (
    <label class="switch">
      <input type="checkbox"> </input>
      <span class="slider round" />
    </label>
  );
}
