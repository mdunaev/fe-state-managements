import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { scan } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PresentationService {
  public isPresentation$: BehaviorSubject<
    boolean
  > = new BehaviorSubject(false);
  constructor() {}

  toggle() {
    this.isPresentation$.next(!this.isPresentation$.getValue());
  }
}
