import { Component, OnInit } from '@angular/core';
import { PresentationService } from '../presentation.service';

@Component({
  selector: 'app-switch-btn',
  templateUrl: './switch-btn.component.html',
  styleUrls: ['./switch-btn.component.css']
})
export class SwitchBtnComponent implements OnInit {
  constructor(private presentation: PresentationService) {}

  ngOnInit() {}

  clickHandler() {
    this.presentation.toggle();
  }
}
