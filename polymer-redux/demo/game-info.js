import {
  PolymerElement,
  html
} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-icon/iron-icon.js';

class GameInfo extends PolymerElement {
  static get template() {
    return html`
      <style>

        :host {
          display: inline-block;
          width: 49%;
          vertical-align: top;
        }
      
        .game-info {
          width: 100%;
          height: 400px;
          background-color: #e4f1fe;
          display: inline-block;
          padding: 10px;
          text-align: center;
          padding-top: 50px;
          box-sizing: border-box;
        }
        
        .is-new {
          color: red;
          font-size: 30px;
        }
        
        .game-name {
          font-size: 50px;
        }
        
        .num-players {
          font-size: 150px;
        }
        
        @media (max-width: 800px) {
          :host {
            width: 100%;
          }
        }      
      </style>
      <div class="game-info">
        <div class="content">
          <div class="is-new">
            new
          </div>
          <div class="game-name">
            Selected Game Name
          </div>

          <div class="num-players">
            420
          </div>
        </div>
      </div>
    `;
  }
  constructor() {
    super();
  }
}

customElements.define('app-game-info', GameInfo);
